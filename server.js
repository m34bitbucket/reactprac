const express = require('express');
const path = require('path');
const fs = require('fs');

const app = express();
const port = process.env.PORT || 3000;

app.use('/', express.static(path.join(__dirname, 'public')));
app.post('/api/img', (req, res) => {
  fs.readdir('./public/assets/img', (err, files) => {
    if (err) throw err;
    res.send(files);
  });
});
app.listen(port, () => {
  console.log('app is running on port', port);
});
