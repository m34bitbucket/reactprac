import React from 'react';
import ReactDOM from 'react-dom';
import { MuiThemeProvider } from 'material-ui/styles';

import Gallery from './components/Gallery.jsx';

ReactDOM.render(
  <MuiThemeProvider>
    <Gallery />
  </MuiThemeProvider>,
  document.querySelector('#root')
);
