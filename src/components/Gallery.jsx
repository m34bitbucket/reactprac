import React from 'react';
import axios from 'axios';
import { GridList, GridTile, Subheader } from 'material-ui';
import ImageZoom from 'react-medium-image-zoom';

import Header from './Header.jsx';

class Gallery extends React.Component {
  componentWillMount() {
    this.setState({
      isLoaded: false
    });
    axios.post('/api/img').then((res) => {
      this.setState({
        imgs: res.data,
        isLoaded: true
      });
    });
  }

  render() {
    if (!this.state.isLoaded) {
      return <Header />
    } else {
      return (
        <article>
          <Header />
          <Subheader>December</Subheader>
          <GridList>
          {
            this.state.imgs.map((img) => {
              return (
                <GridTile key={img}>
                  <ImageZoom
                    image={{
                      src: "/assets/img/" + img,
                    }}
                    zoomImage={{
                      src: "/assets/img/" + img
                    }}
                  />
                </GridTile>
              )
            })
          }
          </GridList>
        </article>
      );
    }
  }
}

export default Gallery;
