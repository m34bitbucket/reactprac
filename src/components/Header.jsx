import React from 'react';
import { AppBar, FontIcon, IconButton } from 'material-ui';

class Header extends React.Component {
  render() {
    return (
      <AppBar
        title="Gallery"
        iconElementLeft={<IconButton><FontIcon className="material-icons">face</FontIcon></IconButton>}
      />
    );
  }
}

export default Header;
